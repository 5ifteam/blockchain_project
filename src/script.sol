pragma solidity ^0.4.17;

contract mortal {
    address owner;
    constructor() public {owner = msg.sender;}
    function kill() public {if (msg.sender == owner) selfdestruct(owner);}
}

contract script is mortal {

    // ##### EVENTS ######

    event newUserCreated();
    event newEstateCreated();
    event newOfferCreated();
    event safeExitTriggered();

    // ##### OBJETS ######
    enum STATUS {Pending_validation, Pending_pay, Completed, Rejected, Countered, Canceled}


    struct User {
        address a;

        string name;
        string tel;

    }

    struct RealEstate {
        address user_address;

        uint id_real_estate;
        uint price;
        uint deposit;
        bool sold;

        string title;
        string adr;
        string image;
        uint date;

        uint[] id_offers;
        uint nbOfOffers_this_estate;
    }

    struct Offer {
        address potential_buyer_address;

        uint id_real_estate;
        uint id_offer;
        uint amount_offered;
        uint counter_offer;
        STATUS sta;

        uint date;
    }

    // ##### TEMPORARY  ######

    // ##### LISTS ######
    mapping(address => User) public users;
    mapping(uint => Offer) public offers;
    mapping(uint => RealEstate) public estates;
    uint public nbOfUsers;
    uint public nbOfOffers;
    uint public nbOfRealEstate;

    // ##### CONSTRUCTORS ######
    function newUser(string user_name, string user_tel) public {
        if (users[msg.sender].a == 0) {
            //User does not exist
            users[msg.sender] = User(msg.sender, user_name, user_tel);
            nbOfUsers += 1;
            emit newUserCreated();
        } else {
            //User already exist
            assert(false);
            // = throw error
        }
    }

    function userExists() public view returns (bool){
        return users[msg.sender].a != 0;
    }

    function newEstate(uint price, uint deposit, string title, string adr, string image) public {
        //TODO : Vérifier que l'user n'a pas déjà créé un estate ? Identique ?

        // Vérifier que l'user existe
        if (users[msg.sender].a == 0) {
            //User does not exist
            assert(false);
            // = throw error
        }

        // Verify correctness of prices
        if (price < deposit) {
            //User does not exist
            assert(false);
            // = throw error
        }

        // Create Estate and dependencies
        RealEstate memory realEstate = RealEstate(msg.sender, nbOfRealEstate, price, deposit, false, title, adr, image, now, new uint[](0), 0);

        // Store it
        estates[realEstate.id_real_estate] = realEstate;

        //Keep updated #
        nbOfRealEstate += 1;

        emit newEstateCreated();
    }

    function newOffer(uint id_real_estate, uint amount_offered) public payable {

        // Verify user does exist
        if (users[msg.sender].a == 0) {
            safeExit();
            require(false, 'User does not exist');
        }

        //RealEstate Can't exist or isn't available
        if (nbOfRealEstate < id_real_estate) {
            safeExit();
            require(false, 'RealEstate id does not exist');
        }

        // Get the estate
        RealEstate memory realEstate = estates[id_real_estate];

        //RealEstate Can't exist or isn't available
        if (realEstate.sold == true) {
            safeExit();
            require(false, 'RealEstate already sold. You cannot bet on it.');
        }

        // Verify user is not the owner of the estate
        if (msg.sender == realEstate.user_address) {
            safeExit();
            require(false, 'Seller cannot bid on its own properties');
        }

        // Verify user gave enough for the security deposit
        if (msg.value != realEstate.deposit) {
            //TODO : send back only the excess of ETH
            safeExit();
            require(false, 'Amount sent does not match security deposit amount.');
        }

        // Create Offer and dependencies
        Offer memory offer_tmp = Offer(msg.sender, id_real_estate, nbOfOffers, amount_offered, 0, STATUS.Pending_validation, now);

        // Store it
        offers[offer_tmp.id_offer] = offer_tmp;

        //Modify the estate to store the offer
        estates[id_real_estate].id_offers.push(nbOfOffers);

        //Keep updated #
        nbOfOffers += 1;
        estates[id_real_estate].nbOfOffers_this_estate += 1;

        emit newOfferCreated();
    }

    function safeExit() internal {
        //Send back the payment
        msg.sender.transfer(msg.value);
        emit safeExitTriggered();
    }

    /*
    function get_estate_as_buyer() public view returns (uint256[]){
        uint256[] memory list;

        for(uint i=0; i < nbOfRealEstate ; i ++){
            list.push(i);
        }

        return list;
    }


        function getEstate(uint id_estate) pure public returns(RealEstate_tmp) {
            RealEstate memory tmp = estates[id_estate];

            RealEstate_tmp memory realEstate_tmp = RealEstate_tmp(tmp.u.name, tmp.u.tel, tmp.id_real_estate, tmp.price, tmp.deposit, tmp.sold, tmp.title, tmp.adr, tmp.image, tmp.date);

            return realEstate_tmp;
        }*/



    // ##### METHODS ######
    function accept_offer_as_seller(uint id_offer) public {
        require(nbOfOffers > id_offer, 'Offer id does not exist');
        require(estates[offers[id_offer].id_real_estate].sold == false, 'Estate already sold.');
        require(msg.sender == estates[offers[id_offer].id_real_estate].user_address, 'Action forbidden without being the seller');
        require(offers[id_offer].sta == STATUS.Pending_validation, 'Action forbidden if the offer is not waiting for validation.');

        //Modify offer objet
        offers[id_offer].sta = STATUS.Pending_pay;
    }

    function reject_offer_as_seller(uint id_offer) public {
        require(nbOfOffers > id_offer, 'Offer id does not exist');
        require(estates[offers[id_offer].id_real_estate].sold == false, 'Estate already sold.');
        require(msg.sender == estates[offers[id_offer].id_real_estate].user_address, 'Action forbidden without being the seller');
        require(offers[id_offer].sta == STATUS.Pending_validation, 'Action forbidden if the offer is not waiting for validation.');

        //Modify offer objet
        offers[id_offer].sta = STATUS.Rejected;
        // GIVE BACK THE DEPOSIT
        offers[id_offer].potential_buyer_address.transfer(estates[offers[id_offer].id_real_estate].deposit);
    }

    function reject_offer_as_seller_counteroffer(uint id_offer, uint counteroffer) public {
        require(nbOfOffers > id_offer, 'Offer id does not exist');
        require(estates[offers[id_offer].id_real_estate].sold == false, 'Estate already sold.');
        require(msg.sender == estates[offers[id_offer].id_real_estate].user_address, 'Action forbidden without being the seller');
        require(offers[id_offer].sta == STATUS.Pending_validation, 'Action forbidden if the offer is not waiting for validation.');

        //Modify offer objet
        offers[id_offer].sta = STATUS.Countered;
        offers[id_offer].counter_offer = counteroffer;
    }

    function complete_payement_as_buyer(uint id_offer) public payable {

        if (nbOfOffers < id_offer) {
            safeExit();
            require(false, 'Offer id does not exist');
        }
        if (estates[offers[id_offer].id_real_estate].sold != false) {
            safeExit();
            require(false, 'Estate already sold.');
        }
        if (msg.sender != offers[id_offer].potential_buyer_address) {
            safeExit();
            require(false, 'Action forbidden without being the buyer.');
        }
        if (offers[id_offer].sta != STATUS.Pending_pay && offers[id_offer].sta != STATUS.Countered) {
            safeExit();
            require(false, 'Action forbidden without offer being validated by seller.');
        }

        uint amount = 0;
        if (offers[id_offer].sta == STATUS.Pending_pay) {
            // Accepted offer
            amount = offers[id_offer].amount_offered;
        } else if(offers[id_offer].sta == STATUS.Countered){
            // Counter offer
            amount = offers[id_offer].counter_offer;
        }

        if (msg.value != amount - estates[offers[id_offer].id_real_estate].deposit) {
            safeExit();
            require(false, 'Action forbidden without correct amount of money sent.');
        }

        offers[id_offer].sta = STATUS.Completed;
        estates[offers[id_offer].id_real_estate].sold = true;
        //Send the price of the house (what sent as deposit + sent to complete the payment)
        estates[offers[id_offer].id_real_estate].user_address.transfer(amount);
    }

    function cancel_payement_as_buyer(uint id_offer) public {
        require(nbOfOffers > id_offer, 'Offer id does not exist');
        // We don't car if the estate si already sold or not // require(estates[offers[id_offer].id_real_estate].sold == false, 'Estate already sold.');
        require(msg.sender == offers[id_offer].potential_buyer_address, 'Action forbidden without being the buyer.');
        // We don't car if the offer had been reviewed by the seller // require(offers[id_offer].sta == STATUS.Pending_pay, 'Action forbidden without offer being validated by seller.');
        require(offers[id_offer].sta != STATUS.Completed, 'Action forbidden on a completed offer.');
        require(offers[id_offer].sta != STATUS.Rejected, 'Action forbidden on a rejected offer.');

        offers[id_offer].sta = STATUS.Canceled;
        //Send the price of the deposit back
        offers[id_offer].potential_buyer_address.transfer(estates[offers[id_offer].id_real_estate].deposit);
    }

    function get_list_as_buyer() public pure {// Pure to remove
        //TODO : Complete
    }

    function get_list_as_seller() public pure {// Pure to remove
        //TODO : Complete
    }

    function get_offers_as_seller(uint id_estate) public view returns (uint[]) {
        require(nbOfRealEstate > id_estate, 'Offer id does not exist');
        require(msg.sender == estates[id_estate].user_address, 'List of offers only accessible for seller');

        /**
        uint[] memory v = new uint[](estates[id_estate].nbOfOffers_this_estate);
        uint counter = 0;
        for (uint i = 0; i < estates[id_estate].nbOfOffers_this_estate; i++) {
            v[counter] = estates[id_estate].id_offers[i];
            counter++;
        }

        return v;
        */

        return estates[id_estate].id_offers;
    }


    // Default function (fallback)
    function() payable public {
        safeExit();
        require(false, 'You called a function that does not exist.');
    }

}