import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

public class RealEstateService {
    public static String create(BigInteger price, BigInteger deposit, String title, String address, String image) {
        return CLI.exponentialBackoff(CLI.smartcontract.newEstate(price, deposit, title, address, image))
                .getTransactionHash();
    }

    public static RealEstate getProperty(int i) {
        return RealEstate.fromTuple(
                CLI.exponentialBackoff(CLI.smartcontract.estates(BigInteger.valueOf(i))));
    }

    public static int getNumberOfProperties() {
        return CLI.exponentialBackoff(CLI.smartcontract.nbOfRealEstate()).intValue();
    }

    public static ArrayList<RealEstate> listProperties() {
        ArrayList<RealEstate> listOfEstate = new ArrayList<>();
        int nbr = getNumberOfProperties();
        for (int i = 0; i < nbr; i++) {
            listOfEstate.add(getProperty(i));
        }
        return listOfEstate;
    }

    public static ArrayList<RealEstate> listProperties(String address){
        ArrayList<RealEstate> props = listProperties();
        props.removeIf((RealEstate x) -> !x.getSellerAddress().equals(address));
        return props;
    }
}
