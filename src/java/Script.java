
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tuples.generated.Tuple10;
import org.web3j.tuples.generated.Tuple3;
import org.web3j.tuples.generated.Tuple7;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.5.0.
 */
public class Script extends Contract {
    private static final String BINARY = "608060405260008054600160a060020a031916331790556123fc806100256000396000f3006080604052600436106101065763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416631267932381146101865780631f34563c146101ad5780632afb9f7c146102445780633e970c0e1461025257806341c0e1b5146102ba5780634fabd15f146102cf5780635c4311a0146102e45780636a64ad9d146102cf5780636f632627146103c057806385fe0562146103d85780638a72ea6a146103f05780639f22620a14610468578063a87430ba1461047d578063a9a54c6714610595578063c04d08a1146105aa578063d70f83c2146105b5578063eae6d4da14610758578063eb78d6cd14610781578063f9e0755614610799575b61010e6107b4565b6040805160e560020a62461bcd02815260206004820152602a60248201527f596f752063616c6c656420612066756e6374696f6e207468617420646f65732060448201527f6e6f742065786973742e00000000000000000000000000000000000000000000606482015290519081900360840190fd5b005b34801561019257600080fd5b5061019b61080c565b60408051918252519081900360200190f35b3480156101b957600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261018494369492936024939284019190819084018382808284375050604080516020601f89358b018035918201839004830284018301909452808352979a9998810197919650918201945092508291508401838280828437509497506108129650505050505050565b6101846004356024356108f9565b34801561025e57600080fd5b5061026a600435610f5f565b60408051602080825283518183015283519192839290830191858101910280838360005b838110156102a657818101518382015260200161028e565b505050509050019250505060405180910390f35b3480156102c657600080fd5b506101846110a1565b3480156102db57600080fd5b506101846110c2565b3480156102f057600080fd5b50604080516020600460443581810135601f810184900484028501840190955284845261018494823594602480359536959460649492019190819084018382808284375050604080516020601f89358b018035918201839004830284018301909452808352979a99988101979196509182019450925082915084018382808284375050604080516020601f89358b018035918201839004830284018301909452808352979a9998810197919650918201945092508291508401838280828437509497506110c49650505050505050565b3480156103cc57600080fd5b506101846004356112ae565b3480156103e457600080fd5b506101846004356114a9565b3480156103fc57600080fd5b50610408600435611705565b6040518088600160a060020a0316600160a060020a0316815260200187815260200186815260200185815260200184815260200183600581111561044857fe5b60ff16815260200182815260200197505050505050505060405180910390f35b34801561047457600080fd5b5061019b611752565b34801561048957600080fd5b5061049e600160a060020a0360043516611758565b6040518084600160a060020a0316600160a060020a031681526020018060200180602001838103835285818151815260200191508051906020019080838360005b838110156104f75781810151838201526020016104df565b50505050905090810190601f1680156105245780820380516001836020036101000a031916815260200191505b50838103825284518152845160209182019186019080838360005b8381101561055757818101518382015260200161053f565b50505050905090810190601f1680156105845780820380516001836020036101000a031916815260200191505b509550505050505060405180910390f35b3480156105a157600080fd5b5061019b61189d565b6101846004356118a3565b3480156105c157600080fd5b506105cd600435611c43565b604051808b600160a060020a0316600160a060020a031681526020018a815260200189815260200188815260200187151515158152602001806020018060200180602001868152602001858152602001848103845289818151815260200191508051906020019080838360005b8381101561065257818101518382015260200161063a565b50505050905090810190601f16801561067f5780820380516001836020036101000a031916815260200191505b5084810383528851815288516020918201918a019080838360005b838110156106b257818101518382015260200161069a565b50505050905090810190601f1680156106df5780820380516001836020036101000a031916815260200191505b50848103825287518152875160209182019189019080838360005b838110156107125781810151838201526020016106fa565b50505050905090810190601f16801561073f5780820380516001836020036101000a031916815260200191505b509d505050505050505050505050505060405180910390f35b34801561076457600080fd5b5061076d611e3c565b604080519115158252519081900360200190f35b34801561078d57600080fd5b50610184600435611e5b565b3480156107a557600080fd5b50610184600435602435611fe4565b60405133903480156108fc02916000818181858888f193505050501580156107e0573d6000803e3d6000fd5b506040517fb757d1179c7346959e453829ed88ce5effd5228791cb7695b1b35580b3c428ee90600090a1565b60045481565b33600090815260016020526040902054600160a060020a031615156108f3576040805160608101825233808252602080830186815283850186905260009283526001808352949092208351815473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03909116178155915180519394929361089d9385019291909101906121e6565b50604082015180516108b99160028401916020909101906121e6565b5050600480546001019055506040517f537f31e525e8edb95da86a7e5435928d04cabacdc8e269d4e3f2bb28b7ea9f7490600090a16108f5565bfe5b5050565b610901612264565b6109096122ca565b33600090815260016020526040902054600160a060020a03161515610980576109306107b4565b6040805160e560020a62461bcd02815260206004820152601360248201527f5573657220646f6573206e6f7420657869737400000000000000000000000000604482015290519081900360640190fd5b8360065410156109e2576109926107b4565b6040805160e560020a62461bcd02815260206004820152601c60248201527f5265616c45737461746520696420646f6573206e6f7420657869737400000000604482015290519081900360640190fd5b6000848152600360208181526040928390208351610160810185528154600160a060020a031681526001808301548285015260028084015483880152948301546060830152600483015460ff161515608083015260058301805487516101009382161593909302600019011695909504601f81018590048502820185019096528581529094919360a086019391929091830182828015610ac35780601f10610a9857610100808354040283529160200191610ac3565b820191906000526020600020905b815481529060010190602001808311610aa657829003601f168201915b505050918352505060068201805460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152938201939291830182828015610b575780601f10610b2c57610100808354040283529160200191610b57565b820191906000526020600020905b815481529060010190602001808311610b3a57829003601f168201915b505050918352505060078201805460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152938201939291830182828015610beb5780601f10610bc057610100808354040283529160200191610beb565b820191906000526020600020905b815481529060010190602001808311610bce57829003601f168201915b505050505081526020016008820154815260200160098201805480602002602001604051908101604052809291908181526020018280548015610c4d57602002820191906000526020600020905b815481526020019060010190808311610c39575b50505050508152602001600a82015481525050915081608001511515600115151415610cf157610c7b6107b4565b6040805160e560020a62461bcd02815260206004820152602e60248201527f5265616c45737461746520616c726561647920736f6c642e20596f752063616e60448201527f6e6f7420626574206f6e2069742e000000000000000000000000000000000000606482015290519081900360840190fd5b8151600160a060020a0316331415610d8157610d0b6107b4565b6040805160e560020a62461bcd02815260206004820152602760248201527f53656c6c65722063616e6e6f7420626964206f6e20697473206f776e2070726f60448201527f7065727469657300000000000000000000000000000000000000000000000000606482015290519081900360840190fd5b60608201513414610e0a57610d946107b4565b6040805160e560020a62461bcd02815260206004820152603360248201527f416d6f756e742073656e7420646f6573206e6f74206d6174636820736563757260448201527f697479206465706f73697420616d6f756e742e00000000000000000000000000606482015290519081900360840190fd5b60e06040519081016040528033600160a060020a0316815260200185815260200160055481526020018481526020016000815260200160006005811115610e4d57fe5b81524260209182015260408281018051600090815260028085529290208451815473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0390911617815592840151600180850191909155905191830191909155606083015160038301556080830151600483015560a08301516005808401805495965086959293909260ff1916918490811115610ee257fe5b021790555060c091909101516006909101556000848152600360209081526040808320600580546009830180546001818101835591885295872090950155805484019055878452600a018054909201909155517fdd765f85f875cc9cde8676f398a8729cc192f257e2b72011cbcfaeadd7bbf0aa9190a150505050565b606081600654111515610faa576040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b600082815260036020526040902054600160a060020a0316331461103e576040805160e560020a62461bcd02815260206004820152602960248201527f4c697374206f66206f6666657273206f6e6c792061636365737369626c65206660448201527f6f722073656c6c65720000000000000000000000000000000000000000000000606482015290519081900360840190fd5b6000828152600360209081526040918290206009018054835181840281018401909452808452909183018282801561109557602002820191906000526020600020905b815481526020019060010190808311611081575b50505050509050919050565b600054600160a060020a03163314156110c257600054600160a060020a0316ff5b565b6110cc612264565b33600090815260016020526040902054600160a060020a031615156110ed57fe5b6101606040519081016040528033600160a060020a0316815260200160065481526020018781526020018681526020016000151581526020018581526020018481526020018381526020014281526020016000604051908082528060200260200182016040528015611169578160200160208202803883390190505b508152600060209182018190528282018051825260038084526040928390208551815473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0390911617815591516001830155918401516002820155606084015191810191909155608083015160048201805460ff191691151591909117905560a0830151805193945084939192611205926005850192909101906121e6565b5060c082015180516112219160068401916020909101906121e6565b5060e0820151805161123d9160078401916020909101906121e6565b506101008201516008820155610120820151805161126591600984019160209091019061231c565b506101409190910151600a909101556006805460010190556040517ff024516b74f6f3f26a11b6952f29a999224ea5177842afd9cb53b4f5224568ab90600090a1505050505050565b60055481106112f5576040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b6000818152600260209081526040808320600101548352600390915290206004015460ff161561135d576040805160e560020a62461bcd0281526020600482015260146024820152600080516020612371833981519152604482015290519081900360640190fd5b60008181526002602090815260408083206001015483526003909152902054600160a060020a031633146113ef576040805160e560020a62461bcd028152602060048201526029602482015260008051602061239183398151915260448201527f68652073656c6c65720000000000000000000000000000000000000000000000606482015290519081900360840190fd5b600081815260026020526040812060059081015460ff169081111561141057fe5b1461148b576040805160e560020a62461bcd02815260206004820152603c60248201527f416374696f6e20666f7262696464656e20696620746865206f6666657220697360448201527f206e6f742077616974696e6720666f722076616c69646174696f6e2e00000000606482015290519081900360840190fd5b6000908152600260205260409020600501805460ff19166001179055565b60055481106114f0576040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b6000818152600260209081526040808320600101548352600390915290206004015460ff1615611558576040805160e560020a62461bcd0281526020600482015260146024820152600080516020612371833981519152604482015290519081900360640190fd5b60008181526002602090815260408083206001015483526003909152902054600160a060020a031633146115ea576040805160e560020a62461bcd028152602060048201526029602482015260008051602061239183398151915260448201527f68652073656c6c65720000000000000000000000000000000000000000000000606482015290519081900360840190fd5b600081815260026020526040812060059081015460ff169081111561160b57fe5b14611686576040805160e560020a62461bcd02815260206004820152603c60248201527f416374696f6e20666f7262696464656e20696620746865206f6666657220697360448201527f206e6f742077616974696e6720666f722076616c69646174696f6e2e00000000606482015290519081900360840190fd5b600081815260026020526040902060050180546003919060ff19166001835b0217905550600081815260026020908152604080832080546001909101548452600392839052818420909201549051600160a060020a039092169281156108fc029290818181858888f193505050501580156108f5573d6000803e3d6000fd5b60026020819052600091825260409091208054600182015492820154600383015460048401546005850154600690950154600160a060020a03909416959492939192909160ff9091169087565b60055481565b600160208181526000928352604092839020805481840180548651600261010097831615979097026000190190911695909504601f8101859004850286018501909652858552600160a060020a03909116949193929091908301828280156118015780601f106117d657610100808354040283529160200191611801565b820191906000526020600020905b8154815290600101906020018083116117e457829003601f168201915b50505060028085018054604080516020601f60001961010060018716150201909416959095049283018590048502810185019091528181529596959450909250908301828280156118935780601f1061186857610100808354040283529160200191611893565b820191906000526020600020905b81548152906001019060200180831161187657829003601f168201915b5050505050905083565b60065481565b60008160055410156118f5576118b76107b4565b6040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b6000828152600260209081526040808320600101548352600390915290206004015460ff1615611965576119276107b4565b6040805160e560020a62461bcd0281526020600482015260146024820152600080516020612371833981519152604482015290519081900360640190fd5b600082815260026020526040902054600160a060020a031633146119ef5761198b6107b4565b6040805160e560020a62461bcd028152602060048201526029602482015260008051602061239183398151915260448201527f68652062757965722e0000000000000000000000000000000000000000000000606482015290519081900360840190fd5b6001600083815260026020526040902060059081015460ff1690811115611a1257fe5b14158015611a4157506004600083815260026020526040902060059081015460ff1690811115611a3e57fe5b14155b15611ac457611a4e6107b4565b6040805160e560020a62461bcd02815260206004820152603960248201527f416374696f6e20666f7262696464656e20776974686f7574206f66666572206260448201527f65696e672076616c6964617465642062792073656c6c65722e00000000000000606482015290519081900360840190fd5b5060006001600083815260026020526040902060059081015460ff1690811115611aea57fe5b1415611b085750600081815260026020526040902060030154611b1c565b506000818152600260205260409020600401545b6000828152600260209081526040808320600101548352600391829052909120015481033414611bc457611b4e6107b4565b6040805160e560020a62461bcd02815260206004820152603660248201527f416374696f6e20666f7262696464656e20776974686f757420636f727265637460448201527f20616d6f756e74206f66206d6f6e65792073656e742e00000000000000000000606482015290519081900360840190fd5b600082815260026020818152604080842060058101805460ff1990811690951790556001908101805486526003909352818520600401805490941617909255548252808220549051600160a060020a039091169183156108fc02918491818181858888f19350505050158015611c3e573d6000803e3d6000fd5b505050565b60036020818152600092835260409283902080546001808301546002808501549685015460048601546005870180548b516101009782161597909702600019011693909304601f8101899004890286018901909a52898552600160a060020a0390951698929796909560ff90951694939290830182828015611d065780601f10611cdb57610100808354040283529160200191611d06565b820191906000526020600020905b815481529060010190602001808311611ce957829003601f168201915b5050505060068301805460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152949594935090830182828015611d965780601f10611d6b57610100808354040283529160200191611d96565b820191906000526020600020905b815481529060010190602001808311611d7957829003601f168201915b5050505060078301805460408051602060026001851615610100026000190190941693909304601f8101849004840282018401909252818152949594935090830182828015611e265780601f10611dfb57610100808354040283529160200191611e26565b820191906000526020600020905b815481529060010190602001808311611e0957829003601f168201915b50505050509080600801549080600a015490508a565b33600090815260016020526040902054600160a060020a031615155b90565b6005548110611ea2576040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b600081815260026020526040902054600160a060020a03163314611f24576040805160e560020a62461bcd028152602060048201526029602482015260008051602061239183398151915260448201527f68652062757965722e0000000000000000000000000000000000000000000000606482015290519081900360840190fd5b6002600082815260026020526040902060059081015460ff1690811115611f4757fe5b1415611fc3576040805160e560020a62461bcd02815260206004820152602660248201527f416374696f6e20666f7262696464656e206f6e206120636f6d706c657465642060448201527f6f666665722e0000000000000000000000000000000000000000000000000000606482015290519081900360840190fd5b60008181526002602052604090206005908101805460ff19166001836116a5565b600554821061202b576040805160e560020a62461bcd02815260206004820152601760248201526000805160206123b1833981519152604482015290519081900360640190fd5b6000828152600260209081526040808320600101548352600390915290206004015460ff1615612093576040805160e560020a62461bcd0281526020600482015260146024820152600080516020612371833981519152604482015290519081900360640190fd5b60008281526002602090815260408083206001015483526003909152902054600160a060020a03163314612125576040805160e560020a62461bcd028152602060048201526029602482015260008051602061239183398151915260448201527f68652073656c6c65720000000000000000000000000000000000000000000000606482015290519081900360840190fd5b600082815260026020526040812060059081015460ff169081111561214657fe5b146121c1576040805160e560020a62461bcd02815260206004820152603c60248201527f416374696f6e20666f7262696464656e20696620746865206f6666657220697360448201527f206e6f742077616974696e6720666f722076616c69646174696f6e2e00000000606482015290519081900360840190fd5b60009182526002602052604090912060058101805460ff191660049081179091550155565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061222757805160ff1916838001178555612254565b82800160010185558215612254579182015b82811115612254578251825591602001919060010190612239565b50612260929150612356565b5090565b610160604051908101604052806000600160a060020a031681526020016000815260200160008152602001600081526020016000151581526020016060815260200160608152602001606081526020016000815260200160608152602001600081525090565b60e0604051908101604052806000600160a060020a03168152602001600081526020016000815260200160008152602001600081526020016000600581111561230f57fe5b8152602001600081525090565b8280548282559060005260206000209081019282156122545791602002820182811115612254578251825591602001919060010190612239565b611e5891905b80821115612260576000815560010161235c560045737461746520616c726561647920736f6c642e000000000000000000000000416374696f6e20666f7262696464656e20776974686f7574206265696e6720744f6666657220696420646f6573206e6f74206578697374000000000000000000a165627a7a72305820efcf65815cd132631c8a02fe31b97045d43439c8c66b24998a37c242ba4412f00029";

    public static final String FUNC_NBOFUSERS = "nbOfUsers";

    public static final String FUNC_NEWUSER = "newUser";

    public static final String FUNC_NEWOFFER = "newOffer";

    public static final String FUNC_GET_OFFERS_AS_SELLER = "get_offers_as_seller";

    public static final String FUNC_KILL = "kill";

    public static final String FUNC_GET_LIST_AS_SELLER = "get_list_as_seller";

    public static final String FUNC_NEWESTATE = "newEstate";

    public static final String FUNC_GET_LIST_AS_BUYER = "get_list_as_buyer";

    public static final String FUNC_ACCEPT_OFFER_AS_SELLER = "accept_offer_as_seller";

    public static final String FUNC_REJECT_OFFER_AS_SELLER = "reject_offer_as_seller";

    public static final String FUNC_OFFERS = "offers";

    public static final String FUNC_NBOFOFFERS = "nbOfOffers";

    public static final String FUNC_USERS = "users";

    public static final String FUNC_NBOFREALESTATE = "nbOfRealEstate";

    public static final String FUNC_COMPLETE_PAYEMENT_AS_BUYER = "complete_payement_as_buyer";

    public static final String FUNC_ESTATES = "estates";

    public static final String FUNC_USEREXISTS = "userExists";

    public static final String FUNC_CANCEL_PAYEMENT_AS_BUYER = "cancel_payement_as_buyer";

    public static final String FUNC_REJECT_OFFER_AS_SELLER_COUNTEROFFER = "reject_offer_as_seller_counteroffer";

    public static final Event NEWUSERCREATED_EVENT = new Event("newUserCreated", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event NEWESTATECREATED_EVENT = new Event("newEstateCreated", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event NEWOFFERCREATED_EVENT = new Event("newOfferCreated", 
            Arrays.<TypeReference<?>>asList());
    ;

    public static final Event SAFEEXITTRIGGERED_EVENT = new Event("safeExitTriggered", 
            Arrays.<TypeReference<?>>asList());
    ;

    protected Script(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Script(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<BigInteger> nbOfUsers() {
        final Function function = new Function(FUNC_NBOFUSERS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> newUser(String user_name, String user_tel) {
        final Function function = new Function(
                FUNC_NEWUSER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(user_name), 
                new org.web3j.abi.datatypes.Utf8String(user_tel)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> newOffer(BigInteger id_real_estate, BigInteger amount_offered, BigInteger weiValue) {
        final Function function = new Function(
                FUNC_NEWOFFER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_real_estate), 
                new org.web3j.abi.datatypes.generated.Uint256(amount_offered)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<List> get_offers_as_seller(BigInteger id_estate) {
        final Function function = new Function(FUNC_GET_OFFERS_AS_SELLER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_estate)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint256>>() {}));
        return new RemoteCall<List>(
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public RemoteCall<TransactionReceipt> kill() {
        final Function function = new Function(
                FUNC_KILL, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void get_list_as_seller() {
        throw new RuntimeException("cannot call constant function with void return type");
    }

    public RemoteCall<TransactionReceipt> newEstate(BigInteger price, BigInteger deposit, String title, String adr, String image) {
        final Function function = new Function(
                FUNC_NEWESTATE, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(price), 
                new org.web3j.abi.datatypes.generated.Uint256(deposit), 
                new org.web3j.abi.datatypes.Utf8String(title), 
                new org.web3j.abi.datatypes.Utf8String(adr), 
                new org.web3j.abi.datatypes.Utf8String(image)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void get_list_as_buyer() {
        throw new RuntimeException("cannot call constant function with void return type");
    }

    public RemoteCall<TransactionReceipt> accept_offer_as_seller(BigInteger id_offer) {
        final Function function = new Function(
                FUNC_ACCEPT_OFFER_AS_SELLER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_offer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> reject_offer_as_seller(BigInteger id_offer) {
        final Function function = new Function(
                FUNC_REJECT_OFFER_AS_SELLER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_offer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>> offers(BigInteger param0) {
        final Function function = new Function(FUNC_OFFERS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(param0)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint8>() {}, new TypeReference<Uint256>() {}));
        return new RemoteCall<Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>>(
                new Callable<Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>>() {
                    @Override
                    public Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger>(
                                (String) results.get(0).getValue(), 
                                (BigInteger) results.get(1).getValue(), 
                                (BigInteger) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue(), 
                                (BigInteger) results.get(4).getValue(), 
                                (BigInteger) results.get(5).getValue(), 
                                (BigInteger) results.get(6).getValue());
                    }
                });
    }

    public RemoteCall<BigInteger> nbOfOffers() {
        final Function function = new Function(FUNC_NBOFOFFERS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Tuple3<String, String, String>> users(String param0) {
        final Function function = new Function(FUNC_USERS, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(param0)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}));
        return new RemoteCall<Tuple3<String, String, String>>(
                new Callable<Tuple3<String, String, String>>() {
                    @Override
                    public Tuple3<String, String, String> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple3<String, String, String>(
                                (String) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (String) results.get(2).getValue());
                    }
                });
    }

    public RemoteCall<BigInteger> nbOfRealEstate() {
        final Function function = new Function(FUNC_NBOFREALESTATE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> complete_payement_as_buyer(BigInteger id_offer, BigInteger weiValue) {
        final Function function = new Function(
                FUNC_COMPLETE_PAYEMENT_AS_BUYER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_offer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function, weiValue);
    }

    public RemoteCall<Tuple10<String, BigInteger, BigInteger, BigInteger, Boolean, String, String, String, BigInteger, BigInteger>> estates(BigInteger param0) {
        final Function function = new Function(FUNC_ESTATES, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(param0)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}, new TypeReference<Bool>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Uint256>() {}, new TypeReference<Uint256>() {}));
        return new RemoteCall<Tuple10<String, BigInteger, BigInteger, BigInteger, Boolean, String, String, String, BigInteger, BigInteger>>(
                new Callable<Tuple10<String, BigInteger, BigInteger, BigInteger, Boolean, String, String, String, BigInteger, BigInteger>>() {
                    @Override
                    public Tuple10<String, BigInteger, BigInteger, BigInteger, Boolean, String, String, String, BigInteger, BigInteger> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple10<String, BigInteger, BigInteger, BigInteger, Boolean, String, String, String, BigInteger, BigInteger>(
                                (String) results.get(0).getValue(), 
                                (BigInteger) results.get(1).getValue(), 
                                (BigInteger) results.get(2).getValue(), 
                                (BigInteger) results.get(3).getValue(), 
                                (Boolean) results.get(4).getValue(), 
                                (String) results.get(5).getValue(), 
                                (String) results.get(6).getValue(), 
                                (String) results.get(7).getValue(), 
                                (BigInteger) results.get(8).getValue(), 
                                (BigInteger) results.get(9).getValue());
                    }
                });
    }

    public RemoteCall<Boolean> userExists() {
        final Function function = new Function(FUNC_USEREXISTS, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> cancel_payement_as_buyer(BigInteger id_offer) {
        final Function function = new Function(
                FUNC_CANCEL_PAYEMENT_AS_BUYER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_offer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> reject_offer_as_seller_counteroffer(BigInteger id_offer, BigInteger counteroffer) {
        final Function function = new Function(
                FUNC_REJECT_OFFER_AS_SELLER_COUNTEROFFER, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(id_offer), 
                new org.web3j.abi.datatypes.generated.Uint256(counteroffer)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<NewUserCreatedEventResponse> getNewUserCreatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(NEWUSERCREATED_EVENT, transactionReceipt);
        ArrayList<NewUserCreatedEventResponse> responses = new ArrayList<NewUserCreatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            NewUserCreatedEventResponse typedResponse = new NewUserCreatedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NewUserCreatedEventResponse> newUserCreatedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NewUserCreatedEventResponse>() {
            @Override
            public NewUserCreatedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(NEWUSERCREATED_EVENT, log);
                NewUserCreatedEventResponse typedResponse = new NewUserCreatedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<NewUserCreatedEventResponse> newUserCreatedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWUSERCREATED_EVENT));
        return newUserCreatedEventObservable(filter);
    }

    public List<NewEstateCreatedEventResponse> getNewEstateCreatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(NEWESTATECREATED_EVENT, transactionReceipt);
        ArrayList<NewEstateCreatedEventResponse> responses = new ArrayList<NewEstateCreatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            NewEstateCreatedEventResponse typedResponse = new NewEstateCreatedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NewEstateCreatedEventResponse> newEstateCreatedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NewEstateCreatedEventResponse>() {
            @Override
            public NewEstateCreatedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(NEWESTATECREATED_EVENT, log);
                NewEstateCreatedEventResponse typedResponse = new NewEstateCreatedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<NewEstateCreatedEventResponse> newEstateCreatedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWESTATECREATED_EVENT));
        return newEstateCreatedEventObservable(filter);
    }

    public List<NewOfferCreatedEventResponse> getNewOfferCreatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(NEWOFFERCREATED_EVENT, transactionReceipt);
        ArrayList<NewOfferCreatedEventResponse> responses = new ArrayList<NewOfferCreatedEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            NewOfferCreatedEventResponse typedResponse = new NewOfferCreatedEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<NewOfferCreatedEventResponse> newOfferCreatedEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, NewOfferCreatedEventResponse>() {
            @Override
            public NewOfferCreatedEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(NEWOFFERCREATED_EVENT, log);
                NewOfferCreatedEventResponse typedResponse = new NewOfferCreatedEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<NewOfferCreatedEventResponse> newOfferCreatedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWOFFERCREATED_EVENT));
        return newOfferCreatedEventObservable(filter);
    }

    public List<SafeExitTriggeredEventResponse> getSafeExitTriggeredEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(SAFEEXITTRIGGERED_EVENT, transactionReceipt);
        ArrayList<SafeExitTriggeredEventResponse> responses = new ArrayList<SafeExitTriggeredEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            SafeExitTriggeredEventResponse typedResponse = new SafeExitTriggeredEventResponse();
            typedResponse.log = eventValues.getLog();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<SafeExitTriggeredEventResponse> safeExitTriggeredEventObservable(EthFilter filter) {
        return web3j.ethLogObservable(filter).map(new Func1<Log, SafeExitTriggeredEventResponse>() {
            @Override
            public SafeExitTriggeredEventResponse call(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(SAFEEXITTRIGGERED_EVENT, log);
                SafeExitTriggeredEventResponse typedResponse = new SafeExitTriggeredEventResponse();
                typedResponse.log = log;
                return typedResponse;
            }
        });
    }

    public Observable<SafeExitTriggeredEventResponse> safeExitTriggeredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(SAFEEXITTRIGGERED_EVENT));
        return safeExitTriggeredEventObservable(filter);
    }

    public static RemoteCall<Script> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Script.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Script> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Script.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static Script load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Script(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Script load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Script(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static class NewUserCreatedEventResponse {
        public Log log;
    }

    public static class NewEstateCreatedEventResponse {
        public Log log;
    }

    public static class NewOfferCreatedEventResponse {
        public Log log;
    }

    public static class SafeExitTriggeredEventResponse {
        public Log log;
    }
}
