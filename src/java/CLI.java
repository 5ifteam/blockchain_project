import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.DefaultGasProvider;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CLI {

    private static String pathToPrivateKey;
    private static String address;
    private static String password;
    private static String smartContractAddress;
    public static Script smartcontract;
    private static Scanner sc = new Scanner(System.in);
    private static Web3j web3;

    public static void main(String[] args) {
        if (!parseArgs(args)) {
            System.err.println("Failed to parse the CLI arguments. Please use this program as follows:");
            System.err.println("java CLI path/to/privatekey \"address\" \"password\" [\"@smart-contract\"]");
            System.exit(1);
        }
        System.out.println("User address: " + address);

        web3 = Web3j.build(new HttpService());

        Credentials credentials = null;
        try {
            credentials = WalletUtils.loadCredentials(password, pathToPrivateKey);
        } catch (Exception e) {
            System.err.println("Failed to load credentials.");
            e.printStackTrace();
            System.exit(1);
        }

        if (smartContractAddress == null) {
            try {
                smartcontract = Script.deploy(web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT).send();
                System.out.println("Successfully deployed a new contract @ " + smartcontract.getContractAddress());
            } catch (Exception e) {
                System.err.println("Error while deploying smart contract.");
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            smartcontract = Script.load(smartContractAddress, web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);
            System.out.println("Successfully loaded the contract @ " + smartcontract.getContractAddress());
        }
        checkAccount();
    }

    public static BigInteger getBalance() {
        try {
            return web3.ethGetBalance(address, DefaultBlockParameterName.LATEST).send().getBalance();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    public static <T> T exponentialBackoff(RemoteCall<T> call) {
        int delay = 1; // in seconds
        while (true) {
            try {
                return call.send();
            } catch (Exception e) {
                if (delay >= 30) {
                    e.printStackTrace();
                    System.exit(1);

                }
                delay *= 2;
            }
            try {
                Thread.sleep(delay * 1000);
            } catch (InterruptedException ie) {
            }
        }

    }

    private static void checkAccount() {
        // If account is not complete, complete it;
        if (!UserService.userExists()) {
            System.out.println("We need some information about you in order for you to be able to use the application:");
            System.out.print("Your name: ");
            String name = sc.nextLine();
            System.out.print("Your phone number: ");
            String phoneNumber = sc.nextLine();
            UserService.createUser(name, phoneNumber);
        }
        menu();
    }


    private static void menu() {
        CLIMenu m = new CLIMenu("Main menu:");

        m.add("Buyer", CLI::buyer);
        m.add("Seller", CLI::seller);
        m.add("Exit", () -> {
        });

        m.display();
    }

    private static void seller() {
        CLIMenu m = new CLIMenu("Seller's menu:");

        m.add("Add a new property for sale", CLI::newProperty);
        m.add("List the offers on your properties", CLI::listSellersOffers);
        m.add("Manage the offers on your properties", CLI::manageSellersOffers);
        m.add("Main menu", CLI::menu);

        m.display();
    }

    private static void manageSellersOffers() {
        System.out.print("Please type the ID of the offer you wish to answer to: ");
        int id = sc.nextInt();
        sc.nextLine();

        Offer o = OfferService.getOffer(id); // TODO handle unknown id? - returns uninitialized object - boolean flag for created?
        RealEstate re = RealEstateService.getProperty(o.getId_real_estate().intValue());

        System.out.println(re);
        System.out.println(o);

        while (true) {
            System.out.print("Your answer to this offer: [A]ccept, [R]eject, [C]ounter-offer: ");
            String choice = sc.nextLine().trim().toUpperCase();
            if (choice.length() != 1)
                continue;
            switch (choice.charAt(0)) {
                case 'A':
                    OfferService.accept(o);
                    break;
                case 'R':
                    OfferService.reject(o);
                    break;
                case 'C':
                    System.out.print("Please type the amount of your counter-offer: ");
                    BigInteger c = sc.nextBigInteger();
                    OfferService.counter(o, c);
                    break;
                default:
                    continue;
            }
            break;
        }
        seller();
    }

    private static void listSellersOffers() {
        ArrayList<RealEstate> myProperties = RealEstateService.listProperties(CLI.address);
        if (myProperties.isEmpty()) {
            System.out.println("You do not have any property for sale.");
        } else {
            for (RealEstate r : myProperties) {
                System.out.println("- " + r);
                List<Offer> offers = OfferService.getOffers(r);
                if (offers.size() == 0) {
                    System.out.println("\t No offers yet on this property.");
                } else {
                    for (Offer o : offers) {
                        System.out.println("\t- " + o);
                    }
                }
            }
        }
        seller();
    }

    private static void newProperty() {
        System.out.println("Adding a new property for sale: ");
        System.out.print("Price: ");
        BigInteger price = sc.nextBigInteger();

        System.out.print("Deposit required to make an offer: ");
        BigInteger deposit = sc.nextBigInteger();
        sc.nextLine(); // consume the \n

        System.out.print("Title: ");
        String title = sc.nextLine();

        System.out.print("Address: ");
        String address = sc.nextLine();

        String image = "NO IMAGE";

        // TODO: are these information valid? => Retry or validate?
        System.out.println("Adding your property to the database...");
        String transactionHash = RealEstateService.create(price, deposit, title, address, image);
        System.out.println("Your property has been added (Transaction hash " + transactionHash + ").");
        seller();
    }

    public static void buyer() {
        CLIMenu m = new CLIMenu("Buyer's menu:");

        m.add("View the properties for sale", CLI::listPropertiesForSale);
        m.add("Make an offer for a property", CLI::makeOffer);
        m.add("List the offers you made", CLI::listBuyersOffers);
        m.add("Manage the offers you made", CLI::manageBuyersOffers);
        m.add("Main menu", CLI::menu);

        m.display();
    }

    private static void manageBuyersOffers() {
        System.out.print("Please type the ID of the offer you wish to handle: ");
        int id = sc.nextInt();
        sc.nextLine();

        Offer o = OfferService.getOffer(id); // TODO handle unknown id?
        RealEstate re = RealEstateService.getProperty(o.getId_real_estate().intValue());

        System.out.println(re);
        System.out.println(o);
        if (!o.getBuyersAddress().equals(address)) {
            System.out.println("You do not own this offer.");
        } else {
            switch (o.getState()) {
                case COUNTERED:
                    handleCounterOfferResponse(o, re);
                    break;
                case WAITING_ANSWER:
                    cancelOffer(o);
                    break;
                case WAITING_PAYMENT:
                    payAsBuyer(o, re);
                    break;
                case REJECTED:
                    makeOffer(re);
                    break;
                default:
                    break;
            }
        }
        buyer();
    }

    private static void cancelOffer(Offer o) {
        System.out.println("Current balance: " + getBalance());
        if (yesNoConfirm("Are you sure you want to cancel your offer and get your deposit back?")) {
            OfferService.cancel(o);
            System.out.println("Your offer has been canceled.");
            System.out.println("Current balance: " + getBalance());
        }
    }

    private static void handleCounterOfferResponse(Offer o, RealEstate re) {
        while (true) {
            System.out.print("Do you wish to [A]ccept this couner-offer or to have your [D]eposit back?: ");
            String choice = sc.nextLine().trim().toUpperCase();
            if (choice.length() != 1)
                continue;
            switch (choice.charAt(0)) {
                case 'A':
                    payAsBuyer(o, re);
                    return;
                case 'D':
                    cancelOffer(o);
                    return;
            }
        }
    }

    private static void payAsBuyer(Offer o, RealEstate re) {
        BigInteger amount = o.getAmountToPay(re.getDeposit());
        System.out.println("Current balance: " + getBalance());
        if (yesNoConfirm("Do you confirm the payment for " + amount + " WEI? (deposit of " + re.getDeposit() + " WEI already paid.)")) {
            OfferService.pay(o, re);
            System.out.println("Your payment has been done.");
            System.out.println("Current balance: " + getBalance());
        }
    }

    private static boolean yesNoConfirm(String s) {
        System.out.println(s + " [Y/N]");
        do {
            System.out.print("> ");
            String a = sc.nextLine().trim().toUpperCase();
            if (a.length() == 1) {
                switch (a.charAt(0)) {
                    case 'Y':
                        return true;
                    case 'N':
                        return false;
                }
            }
        } while (true);
    }

    private static void listBuyersOffers() {
        List<Offer> os = OfferService.getBuyersOffers(address);
        for (Offer o : os)
            System.out.println(o);
        buyer();
    }

    private static void makeOffer() {
        System.out.println("Making an offer on a property: ");

        System.out.print("Identifier of the property: ");
        int id = sc.nextInt();

        RealEstate a = RealEstateService.getProperty(id);
        makeOffer(a);
    }

    private static void makeOffer(RealEstate a) {
        System.out.println("By making an offer on this property, you will pay a deposit of " + a.getDeposit() + " WEI.");
        System.out.println("The seller asks for " + a.getPrice() + " WEI for this property. How much do you wish to pay?");
        System.out.print("> ");
        BigInteger amount = sc.nextBigInteger();

        System.out.println("Creating the offer...");
        String h = OfferService.createOffer(a, amount);
        System.out.println("Your offer has been added (Transaction hash " + h + ").");
        buyer();
    }

    private static void listPropertiesForSale() {
        List<RealEstate> a = RealEstateService.listProperties()
                .stream().filter(x -> !x.isSold()).collect(Collectors.toList());
        if (a.isEmpty()) {
            System.out.println("No properties are available right now, please come back later.");
        } else {
            System.out.println("List of the available properties: ");
            for (RealEstate re : a) {
                System.out.println(re);
            }
        }
        buyer();
    }

    private static boolean parseArgs(String[] args) {
        if (args.length < 3 || args.length > 4) {
            return false;
        }
        pathToPrivateKey = args[0];
        address = args[1];
        if (!address.startsWith("0x"))
            address = "0x" + address;
        password = args[2];
        if (args.length == 4)
            smartContractAddress = args[3];
        return true;
    }
}
