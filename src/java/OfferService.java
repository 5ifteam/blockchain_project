import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OfferService {
    public static String createOffer(RealEstate re, BigInteger amountOffer) {
        return CLI.exponentialBackoff(
                CLI.smartcontract.newOffer(re.getId(), amountOffer, re.getDeposit())
        ).getTransactionHash();
    }

    public static Offer getOffer(int id) {
        return Offer.fromTuple(CLI.exponentialBackoff(CLI.smartcontract.offers(BigInteger.valueOf(id))));
    }

    public static List<Offer> getOffers(RealEstate r) {
        return (List<Offer>) CLI.exponentialBackoff(CLI.smartcontract.get_offers_as_seller(r.getId())).stream()
                .map(x -> getOffer(((BigInteger) x).intValue())).collect(Collectors.toList());
    }

    private static Stream<Offer> getOffers() {
        int nbOffers = CLI.exponentialBackoff(CLI.smartcontract.nbOfOffers()).intValue();
        return Stream.iterate(0, x -> x + 1).limit(nbOffers).map(OfferService::getOffer);
    }

    public static List<Offer> getBuyersOffers(String address) {
        return getOffers().filter(x -> x.getBuyersAddress().equals(address)).collect(Collectors.toList());
    }

    public static String accept(Offer o) {
        return CLI.exponentialBackoff(CLI.smartcontract.accept_offer_as_seller(o.getId_offer())).getTransactionHash();
    }

    public static String reject(Offer o) {
        return CLI.exponentialBackoff(CLI.smartcontract.reject_offer_as_seller(o.getId_offer())).getTransactionHash();
    }

    public static String counter(Offer o, BigInteger i) {
        return CLI.exponentialBackoff(CLI.smartcontract.reject_offer_as_seller_counteroffer(o.getId_offer(), i))
                .getTransactionHash();
    }

    public static String pay(Offer o, RealEstate re) {
        return CLI.exponentialBackoff(CLI.smartcontract.complete_payement_as_buyer(o.getId_offer(), o.getAmountToPay(re.getDeposit())))
                .getTransactionHash();
    }

    public static String cancel(Offer o) {
        return CLI.exponentialBackoff(CLI.smartcontract.cancel_payement_as_buyer(o.getId_offer())).getTransactionHash();
    }

}
