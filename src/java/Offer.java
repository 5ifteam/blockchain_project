import org.web3j.tuples.generated.Tuple7;

import java.math.BigInteger;

public class Offer {
    enum State {WAITING_ANSWER, WAITING_PAYMENT, COMPLETED, REJECTED, COUNTERED, CANCELED}

    private final State[] STATE_MAP_CACHE = State.values();

    private String buyersAddress;
    private BigInteger id_real_estate;
    private BigInteger id_offer;
    private BigInteger amount_offered;
    private BigInteger amount_counter_offer;
    private State state;
    private BigInteger date;

    public Offer(String buyersAddress, BigInteger id_real_estate, BigInteger id_offer, BigInteger amount_offered, BigInteger amount_counter_offer, BigInteger state, BigInteger date) {
        this.buyersAddress = buyersAddress;
        this.id_real_estate = id_real_estate;
        this.id_offer = id_offer;
        this.amount_offered = amount_offered;
        this.amount_counter_offer = amount_counter_offer;
        this.state = STATE_MAP_CACHE[state.intValue()];
        this.date = date;
    }

    public BigInteger getAmountToPay(BigInteger deposit) {
        return (getState() == State.COUNTERED ? getAmount_counter_offer() : getAmount_offered()).subtract(deposit);
    }

    public String getBuyersAddress() {
        return buyersAddress;
    }

    public BigInteger getAmount_counter_offer() {
        return amount_counter_offer;
    }

    public BigInteger getId_real_estate() {
        return id_real_estate;
    }

    public BigInteger getId_offer() {
        return id_offer;
    }

    public BigInteger getAmount_offered() {
        return amount_offered;
    }

    public State getState() {
        return state;
    }

    public BigInteger getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id_real_estate=" + id_real_estate +
                ", id_offer=" + id_offer +
                ", amount_offered=" + amount_offered +
                ", amount_counter_offer=" + amount_counter_offer +
                ", state=" + state +
                ", date=" + date +
                '}';
    }

    public static Offer fromTuple(Tuple7<String, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> t) {
        return new Offer(t.getValue1(),
                t.getValue2(),
                t.getValue3(),
                t.getValue4(),
                t.getValue5(),
                t.getValue6(),
                t.getValue7());
    }
}
