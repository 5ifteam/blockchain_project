import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.DefaultGasProvider;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Launcher {

    private static String SELLER_PASSWORD;
    private static String SELLER_FILE;
    private static String SELLER_ADDRESS;

    private static String BUYER_PASSWORD;
    private static String BUYER_FILE;
    private static String BUYER_ADDRESS;

    //private static final String SELLER_FILE = "/home/zettanux/.ethereum/VincentChain/keystore/UTC--2018-10-08T07-07-52.851944326Z--7c1810b0318900ee5fc5dd5a35bb5c03f564556c";
    //private static final String SELLER_PASSWORD = "08101996123456";

    private static EthFilter filter;

    public static void loadConfig() {
        Properties prop = new Properties();

        String fileName = "app.config";
        ClassLoader classLoader = Launcher.class.getClassLoader();

        URL res = classLoader.getResource(fileName);
        try {
            InputStream is = new FileInputStream(res.getFile());
            prop.load(is);
        } catch (Exception e) {
            System.err.println("Please create the file/src/java/app.config from the template /src/java/app.config.template.");
            System.exit(1);
        }

        SELLER_PASSWORD = prop.getProperty("seller.password");
        SELLER_FILE = prop.getProperty("seller.file");
        SELLER_ADDRESS = prop.getProperty("seller.address");

        BUYER_PASSWORD = prop.getProperty("buyer.password");
        BUYER_FILE = prop.getProperty("buyer.file");
        BUYER_ADDRESS = prop.getProperty("buyer.address");

    }

    public static void main(String[] args) throws Exception {
        loadConfig();
        System.out.println("=== Initialisation ===");
        // Connect to local node
        Web3j web3 = Web3j.build(new HttpService());  // defaults to http://localhost:8545/

        // Load credentials for accessing wallet of source account
        Credentials credentials = WalletUtils.loadCredentials(SELLER_PASSWORD, SELLER_FILE);

        // Deploy the contract in the blockchain
        Script smartcontract = Script.deploy(web3, credentials, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT).send();

        filter = new EthFilter(DefaultBlockParameterName.EARLIEST, DefaultBlockParameterName.LATEST, smartcontract.getContractAddress());

        System.out.println("******************************* SELLER ACTIVITY ***********************************");
        // Create an account
        create_user(smartcontract, "Jackie", "1234567891");

        // Create real-estate
        create_realEstate(smartcontract, 400, 20, "beautiful house", "7 rue soleil", "/img");
        create_realEstate(smartcontract, 170, 2, "home", "7 rue foret", "/img");

        // Print list of my real-estates
        ArrayList<RealEstate> my_estates = get_estates_as_seller(smartcontract, SELLER_ADDRESS);
        for (RealEstate i : my_estates) {
            System.out.println(i.toString());
        }

        System.out.println("******************************* END SELLER ACTIVITY ***********************************");
        System.out.println("******************************* BUYER ACTIVITY ***********************************");

        // Load his instance of the smart contract
        Credentials credentials_buyer = WalletUtils.loadCredentials(BUYER_PASSWORD, BUYER_FILE);
        Script sm_buyer = Script.load(smartcontract.getContractAddress(), web3, credentials_buyer, DefaultGasProvider.GAS_PRICE, DefaultGasProvider.GAS_LIMIT);

        // creates an account
        create_user(sm_buyer, "Harvey", "876555543");


        // Print list of all real-estates
        ArrayList<RealEstate> estates = get_estates(sm_buyer);
        for (RealEstate i : estates) {
            System.out.println(i.toString());
        }


        // Make an offer on the first announce
        create_offer(sm_buyer, 0, 20, BigInteger.valueOf(20));
        create_offer(sm_buyer, 1, 2, BigInteger.valueOf(2));
        // Print my offers
        ArrayList<Offer> my_offers = get_offers_as_buyer(smartcontract, BUYER_ADDRESS);
        if (!my_offers.isEmpty()) {
            for (Offer i : my_offers) {
                System.out.println(i.toString());
            }
        }

        System.out.println("******************************* END BUYER ACTIVITY ***********************************");

        ArrayList<Offer> my_offers_seller = get_offers_as_seller(smartcontract, SELLER_ADDRESS);
        if (!my_offers.isEmpty()) {
            for (Offer i : my_offers_seller) {
                System.out.println(i.toString());
            }
        }


        smartcontract.kill().send();
        System.exit(0);
    }


    public static void create_realEstate(Script smartcontract, long price, long deposit, String title, String address, String image) throws Exception {
        smartcontract.newEstate(BigInteger.valueOf(price), BigInteger.valueOf(deposit), title, address, image).send();
        smartcontract.newEstateCreatedEventObservable(filter).subscribe(tx ->
                System.out.println("Real-Estate Created !"));
    }

    public static ArrayList<RealEstate> get_estates(Script smartcontract) throws Exception {

        ArrayList<RealEstate> listOfEstate = new ArrayList<>();
        for (int i = 0; i < smartcontract.nbOfRealEstate().send().intValue(); i++) {

            listOfEstate.add(getEstate(smartcontract, i));

        }
        return listOfEstate;
    }

    public static ArrayList<RealEstate> get_estates_as_seller(Script smartcontract, String address) throws Exception {
        ArrayList<RealEstate> listOfEstate = new ArrayList<>();
        for (int i = 0; i < smartcontract.nbOfRealEstate().send().intValue(); i++) {
            RealEstate re = getEstate(smartcontract, i);
            if (re.getSellerAddress().equals(address)) {
                listOfEstate.add(re);
            }
        }
        return listOfEstate;
    }

    public static RealEstate getEstate(Script smartcontract, int i) throws Exception {
        return RealEstate.fromTuple(smartcontract.estates(BigInteger.valueOf(i)).send());
    }


    public static void create_user(Script smartcontract, String name, String tel) throws Exception {
        // create the user
        smartcontract.newUser(name, tel).send();
        // handle the event
        smartcontract.newUserCreatedEventObservable(filter).subscribe(tx ->
                System.out.println("User Created !"));
    }

    public static void create_offer(Script smartcontract, int id_estate, long amount_offered, BigInteger wei) throws Exception {
        // create the offer
        smartcontract.newOffer(BigInteger.valueOf(id_estate), BigInteger.valueOf(amount_offered), wei).send();
        // handle the event
        smartcontract.newOfferCreatedEventObservable(filter).subscribe(tx ->
                System.out.println("Offer Created !"));
    }

    public static ArrayList<Offer> get_offers_as_buyer(Script smartcontract, String address) throws Exception {

        ArrayList<Offer> listOfOffers = new ArrayList<>();

        for (int i = 0; i < smartcontract.nbOfOffers().send().intValue(); i++) {

            if (smartcontract.offers(BigInteger.valueOf(i)).send().getValue1().equals(address)) {

                listOfOffers.add(get_offer(smartcontract, BigInteger.valueOf(i)));
            }

        }
        return listOfOffers;

    }

    public static ArrayList<Offer> get_offers_as_seller(Script smartcontract, String address) throws Exception {

        ArrayList<RealEstate> estates = get_estates_as_seller(smartcontract, address);
        ArrayList<Offer> listOfOffers = new ArrayList<Offer>();

        for (RealEstate r : estates){

            List list_id = smartcontract.get_offers_as_seller(r.getId()).send();

            for (int i=0; i < list_id.size(); i++){

                listOfOffers.add(get_offer(smartcontract, (BigInteger)  list_id.get(i) ));
            }
        }
        return listOfOffers;
    }

    public static Offer get_offer(Script smartcontract, BigInteger i) throws Exception {
        return Offer.fromTuple(smartcontract.offers(i).send());
    }

    public static void accept_offer_as_seller(Script smartcontract, int i){
        smartcontract.accept_offer_as_seller(BigInteger.valueOf(i));
    }

    public static void reject_offer_as_seller(Script smartcontract, int i){
        smartcontract.reject_offer_as_seller(BigInteger.valueOf(i));
    }
    public static void complete_payement_as_buyer(Script smartcontract, int i, int wei ){
        smartcontract.complete_payement_as_buyer(BigInteger.valueOf(i), BigInteger.valueOf(wei));
    }
    public static void cancel_payement_as_buyer(Script smartcontract, int i ){
        smartcontract.cancel_payement_as_buyer(BigInteger.valueOf(i));
    }
}

