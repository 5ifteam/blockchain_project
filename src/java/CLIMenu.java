import java.util.ArrayList;
import java.util.Scanner;

public class CLIMenu {
    ArrayList<String> txts;
    ArrayList<Runnable> fns;
    String name;

    public CLIMenu(String name) {
        txts = new ArrayList<>();
        fns = new ArrayList<>();
        this.name = name;
    }

    public void add(String txt, Runnable r) {
        txts.add(txt);
        fns.add(r);
    }

    public void display() {
        System.out.println(name);
        for (int i = 0; i < txts.size(); i++) {
            System.out.println((i + 1) + " - " + txts.get(i));
        }
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            System.out.print("> ");
            choice = sc.nextInt();
        } while (choice < 1 || choice > txts.size());
        sc.nextLine();
        fns.get(choice-1).run();
    }
}
