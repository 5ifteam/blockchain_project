public class UserService {

    public static boolean userExists() {
        return CLI.exponentialBackoff(CLI.smartcontract.userExists());
    }

    public static String createUser(String name, String phoneNumber) {
        return CLI.exponentialBackoff(CLI.smartcontract.newUser(name, phoneNumber)).getTransactionHash();
    }
}
