import org.web3j.tuples.generated.Tuple10;

import java.math.BigInteger;

public class RealEstate {

    private BigInteger id;
    private BigInteger price;
    private BigInteger deposit;
    private boolean sold;

    private String sellerAddress;
    private String title;
    private String address;
    private String image;
    private BigInteger date;


    public RealEstate(BigInteger id, BigInteger price, BigInteger deposit, boolean sold, String sellerAddress, String title, String address, String image, BigInteger date) {
        this.id = id;

        this.price = price;
        this.deposit = deposit;
        this.sold = sold;
        this.sellerAddress = sellerAddress;
        this.title = title;
        this.address = address;
        this.image = image;
        this.date = date;
    }

    public BigInteger getId() {
        return id;
    }

    public BigInteger getPrice() {
        return price;
    }

    public BigInteger getDeposit() {
        return deposit;
    }

    public boolean isSold() {
        return sold;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public String getImage() {
        return image;
    }

    public BigInteger getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "RealEstate{" +
                "id=" + id +
                ", price=" + price +
                ", deposit=" + deposit +
                ", sold=" + sold +
                ", sellerAddress='" + sellerAddress + '\'' +
                ", title='" + title + '\'' +
                ", address='" + address + '\'' +
                ", image='" + image + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    public static RealEstate fromTuple(Tuple10<String, BigInteger, BigInteger, BigInteger,
            Boolean, String, String, String, BigInteger, BigInteger> t) {
        return new RealEstate(t.getValue2(),
                t.getValue3(),
                t.getValue4(),
                t.getValue5(),
                t.getValue1(),
                t.getValue6(),
                t.getValue7(),
                t.getValue8(),
                t.getValue9());
    }

}
