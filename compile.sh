#!/bin/bash
set -x
mkdir -p target \
 && solc src/script.sol --bin --abi --optimize -o target/ --overwrite \
 && web3j solidity generate target/script.bin target/script.abi -o src/java/ -p "default" \
 && sed '1d' src/java/default/Script.java > src/java/Script.java \
 && rm -r src/java/default/
